﻿using BusinessObjects.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Repositories;
using Repositories.Interface;

namespace IdentityAJaxClient.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private IAspNetUserRepository repository = new AspNetUserRepository();
        [HttpGet]
        public ActionResult<IEnumerable<AspNetUser>> GetUsers() => repository.GetUsers();
        [HttpGet("{id}")]
        public ActionResult<AspNetUser> GetUserById(int id) => repository.GetUserById(id);
        [HttpGet("Search/{keyword}")]
        public ActionResult<IEnumerable<AspNetUser>> Search(string keyword) => repository.Search(keyword);
        [HttpGet("Email/{email}")]
        public ActionResult<AspNetUser> GetUserByEmail(string email) => repository.GetUserByEmail(email);
        [HttpPost]
        public IActionResult PostMember(AspNetUser memberRequest)
        {
            var member = new AspNetUser
            {
                UserName = memberRequest.UserName,
                NormalizedUserName = memberRequest.NormalizedUserName,
                Email = memberRequest.Email,
                NormalizedEmail = memberRequest.NormalizedEmail,
                EmailConfirmed = memberRequest.EmailConfirmed,
                PasswordHash = memberRequest.PasswordHash,
                SecurityStamp = memberRequest.SecurityStamp,
                ConcurrencyStamp = memberRequest.ConcurrencyStamp,
                PhoneNumber = memberRequest.PhoneNumber,
                PhoneNumberConfirmed = memberRequest.PhoneNumberConfirmed,
                TwoFactorEnabled = memberRequest.TwoFactorEnabled,
                LockoutEnd = memberRequest.LockoutEnd,
                LockoutEnabled = memberRequest.LockoutEnabled,
                AccessFailedCount = memberRequest.AccessFailedCount,
                FirstName = memberRequest.FirstName,
                LastName = memberRequest.LastName,
            };
            repository.SaveUser(member);
            return NoContent();
        }
        [HttpDelete("{id}")]
        public IActionResult DeleteMember(int id)
        {
            var c = repository.GetUserById(id);
            if (c == null)
            {
                return NotFound();
            }
            repository.DeleteUser(c);
            return NoContent();
        }

        [HttpPut("{id}")]
        public IActionResult PutCustomer(int id, AspNetUser memberRequest)
        {
            var cTmp = repository.GetUserById(id);
            if (cTmp == null)
            {
                return NotFound();
            }

            cTmp.UserName = memberRequest.UserName;
            cTmp.NormalizedUserName = memberRequest.NormalizedUserName;
            cTmp.Email = memberRequest.Email;
            cTmp.NormalizedEmail = memberRequest.NormalizedEmail;
            cTmp.EmailConfirmed = memberRequest.EmailConfirmed;
            cTmp.PasswordHash = memberRequest.PasswordHash;
            cTmp.SecurityStamp = memberRequest.SecurityStamp;
            cTmp.ConcurrencyStamp = memberRequest.ConcurrencyStamp;
            cTmp.PhoneNumber = memberRequest.PhoneNumber;
            cTmp.PhoneNumberConfirmed = memberRequest.PhoneNumberConfirmed;
            cTmp.TwoFactorEnabled = memberRequest.TwoFactorEnabled;
            cTmp.LockoutEnd = memberRequest.LockoutEnd;
            cTmp.LockoutEnabled = memberRequest.LockoutEnabled;
            cTmp.AccessFailedCount = memberRequest.AccessFailedCount;
            cTmp.FirstName = memberRequest.FirstName;
            cTmp.LastName = memberRequest.LastName;

            if (memberRequest.PasswordHash != null && cTmp.PasswordHash != memberRequest.PasswordHash)
            {
                cTmp.PasswordHash = memberRequest.PasswordHash;
            }

            repository.UpdateUser(cTmp);
            return NoContent();
        }
    }
}

