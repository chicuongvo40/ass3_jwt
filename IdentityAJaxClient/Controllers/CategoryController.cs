﻿using BusinessObjects.Models;
using Microsoft.AspNetCore.Mvc;
using Repositories.Interface;
using Repositories;
using Microsoft.AspNetCore.Authorization;

namespace IdentityAJaxClient.Controllers
{
    [Authorize(Roles = "Admin")]
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private ICategoryRepository repository = new CategoryRepository();

        [HttpGet]
        public ActionResult<IEnumerable<Category>> GetCategories() => repository.GetCategories();

        [HttpPost]
        public IActionResult PostCategory(Category category)
        {
            repository.SaveCategory(category);
            return NoContent();
        }
    }
}