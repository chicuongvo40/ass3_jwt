﻿using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Mvc;
using Repositories.Interface;
using Repositories;
using BusinessObjects.Models;
using Microsoft.AspNetCore.Authorization;
using System.Data;

namespace IdentityAJaxClient.Controllers
{
    [Authorize(Roles = "Admin")]
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private IProductRepository repository = new ProductRepository();
        //Get: apu/Products
        [HttpGet]
        public ActionResult<IEnumerable<Product>> GetProducts() => repository.GetProducts();
        [HttpGet("{id}")]
        public ActionResult<Product> GetProductById(int id) => repository.GetProductById(id);
        //Post : ProductsController/Products
        [HttpPost]
        public IActionResult PostProduct(Product productRequest)
        {
            var p = new Product
            {
                ProductName = productRequest.ProductName,
                Weight = productRequest.Weight,
                CategoryId = productRequest.CategoryId,
                UnitPrice = productRequest.UnitPrice,
                UnitslnStock = productRequest.UnitslnStock
            };
            repository.SaveProduct(p);
            return NoContent();
        }

        //Get: ProductsController/Delete/5
        [HttpDelete("{id}")]
        public IActionResult DeleteProduct(int id)
        {
            var p = repository.GetProductById(id);
            if (p == null)
                return NotFound();
            repository.DeleteProduct(p);
            return NoContent();
        }
        [HttpPut("{id}")]
        public IActionResult UpdateProduct(int id, Product productRequest)
        {
            var pTmp = repository.GetProductById(id);
            if (pTmp == null)
                return NotFound();
            pTmp.ProductName = productRequest.ProductName;
            pTmp.Weight = productRequest.Weight;
            pTmp.CategoryId = productRequest.CategoryId;
            pTmp.UnitPrice = productRequest.UnitPrice;
            pTmp.UnitslnStock = productRequest.UnitslnStock;
            repository.UpdateProduct(pTmp);
            return NoContent();
        }
    }
}
