﻿using BusinessObjects.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class ProductDAO
    {
        public static List<Product> GetProducts()
        {
            var listProducts = new List<Product>();
            try
            {
                using (var context = new MyStoreDBContext()
                )
                {
                    listProducts.AddRange(context.Products);
                };
            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }
            return listProducts;
        }
        public static Product FindProductById(int proId)
        {
            Product p = new Product();
            try
            {
                using (var context = new MyStoreDBContext())
                {
                    p = context.Products.SingleOrDefault(x => x.ProductId == proId);
                }
            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }
            return p;
        }
        public static List<Product> Search(string keyword)
        {
            var listProduct = new List<Product>();
            try
            {
                using (var context = new MyStoreDBContext())
                {
                    listProduct = context.Products.Where(f => f.ProductName.Contains(keyword)).ToList();
                    listProduct.ForEach(f =>
                    {
                        f.Category = context.Categories.Find(f.CategoryId);
                    });
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return listProduct;
        }
        public static void SaveProduct(Product p)
        {
            try
            {
                using (var context = new MyStoreDBContext())
                {
                    context.Products.Add(p);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }
        }
        public static void UpdateProduct(Product p)
        {
            try
            {
                using (var context = new MyStoreDBContext())
                {
                    context.Entry<Product>(p).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }
        }
        public static Product DeleteProduct(Product p)
        {
            try
            {
                using (var context = new MyStoreDBContext())
                {
                    var p1 = context.Products.SingleOrDefault(c => c.ProductId == p.ProductId);
                    context.Products.Remove(p1);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }
            return p;
        }
    }
}
