﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class AspNetRoleClaim
    {
        public int Id { get; set; }
        public int? RoleId { get; set; }
        public string ClaimType { get; set; } = null!;
        public string ClaimValue { get; set; } = null!;

        public virtual AspNetRole? Role { get; set; }
    }
}
