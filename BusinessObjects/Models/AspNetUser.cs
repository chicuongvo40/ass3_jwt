﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class AspNetUser
    {
        public AspNetUser()
        {
            AspNetUserClaims = new HashSet<AspNetUserClaim>();
            Orders = new HashSet<Order>();
        }

        public int Id { get; set; }
        public string UserName { get; set; } = null!;
        public string NormalizedUserName { get; set; } = null!;
        public string Email { get; set; } = null!;
        public string NormalizedEmail { get; set; } = null!;
        public string EmailConfirmed { get; set; } = null!;
        public string PasswordHash { get; set; } = null!;
        public string SecurityStamp { get; set; } = null!;
        public string ConcurrencyStamp { get; set; } = null!;
        public string PhoneNumber { get; set; } = null!;
        public string PhoneNumberConfirmed { get; set; } = null!;
        public string TwoFactorEnabled { get; set; } = null!;
        public string LockoutEnd { get; set; } = null!;
        public string LockoutEnabled { get; set; } = null!;
        public string AccessFailedCount { get; set; } = null!;
        public string FirstName { get; set; } = null!;
        public string LastName { get; set; } = null!;

        public virtual ICollection<AspNetUserClaim> AspNetUserClaims { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
    }
}
