﻿using BusinessObjects.Models;
using DataAccess;
using Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories
{
    public class OrderRepository : IOrderRepository
    {
        public void DeleteOrder(Order order)
       => OrderDAO.DeleteOrder(order);

        public List<Order> GetAllOrdersByMemberId(int memberId)
        => OrderDAO.FindAllOrdersByuserId(memberId);

        public Order GetOrderById(int id)
      => OrderDAO.FindOrderById(id);

        public List<Order> GetOrders()
      => OrderDAO.GetOrders();

        public Order SaveOrder(Order order)
      => OrderDAO.SaveOrder(order);

        public void UpdateOrder(Order order)
      => OrderDAO.UpdateOrder(order);
    }
}
